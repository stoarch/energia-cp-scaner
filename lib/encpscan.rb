require_relative 'energia-cp-scaner'
require 'logger'
require 'ap'

CP_ZABADAM_OTKORM = 4403 
CP_ZUM = 4113
CP_KAIPAS = 4114
CP_NSUZNAYA = 4115

$cps = [CP_ZABADAM_OTKORM, CP_ZUM, CP_KAIPAS, CP_NSUZNAYA]
$log = Logger.new('encpscan.log','daily')

loop do
	begin
		scan = EnergiaCPointScaner.new
		puts 'Scaning...'
		$cps.each do |cp|
			scan.query( cp )
			puts "CP #{cp} value: #{scan.value} #{scan.datetime}"
		end

		puts "Waiting..."
		300.times do 
			print '.'
			sleep 1 
		end
	rescue Exception => ex
		ap ex.message
		ap ex.backtrace[0..10]
		$log.error ex.message
		$log.info ex.backtrace
	end
end
