require 'net/http'
require 'json'
require 'bunny'

class EnergiaCPointScaner
	attr_reader :value
	attr_reader :datetime

	def last_value( code )
		retval = Net::HTTP.get(URI("http://92.47.27.114:9293/devices/#{code}/cur-values"))
		return 0.0 if retval.nil?

		val = JSON.parse(retval)
		@value = -1.0
		@datetime = DateTime.now.strftime("%Y.%m.%d %H:%M:%S")
		return nil if val["values"] == []
		@value = val["values"][0]["content"].to_f.round(2)
		@datetime = val["values"][0]["attributes"]["time"]
		@value
	end

	def query( code )
		val = last_value( code )
		
		if val.nil?
			puts "No value for #{code} #{@datetime}"
		end
		
		con = Bunny.new("amqp://ecpscan:esc23Ac@92.47.27.114:5672")
		con.start

		ch = con.create_channel
		x = ch.fanout("cpvals")
		ch.queue("", exclusive: true, arguments: {"x-message-ttl" => 10000})

		x.publish( "#{code} #{@value} #{@datetime}")

		con.close
	end
end
