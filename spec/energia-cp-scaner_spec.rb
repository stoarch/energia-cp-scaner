require File.expand_path(File.dirname(__FILE__) + '/spec_helper')
require 'bunny'

describe EnergiaCPointScaner do
	subject { EnergiaCPointScaner.new }
  it "should read cpoint 4403 value from server" do
		val = subject.last_value( 4403 )
		val.should_not be_nil 
		val.should == 6.32 
		subject.value.should == 6.32
		subject.datetime.should == '31.05.2016 14:54:42'
  end

	it "should not read cpoint 4111 value from server" do
		val = subject.last_value( 4111 )
		val.should be_nil 
	end

	it "should send value to rabbit mq" do
		con = Bunny.new("amqp://cpsaver:cs33A8qZ@92.47.27.114:5672")
		con.start
		ch = con.create_channel
		x = ch.fanout("cpvals")

		val = ''

		ch.queue("cpsaver", auto_delete: true).bind(x).subscribe do |deliver_info, metadata, payload|
			val = payload	
		end

		subject.query( 4403 )
		con.close

		val.should == '4403 6.32 31.05.2016 14:54:42'
	end
end
